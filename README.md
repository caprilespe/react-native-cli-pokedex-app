# Description
This is a Pokedex react native ⚛️ 📱 with react native cli application that integrated with pokeapi to show the list and information of the pokemon

## Make With
[![React Native](https://img.shields.io/badge/React%20Native-5ccfee?style=for-the-badge&logo=react&logoColor=white&labelColor=000000)]()
[![JavaScript](https://img.shields.io/badge/javascript-ead547?style=for-the-badge&logo=javascript&logoColor=white&labelColor=000000)]()